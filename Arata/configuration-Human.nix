{ config, pkgs, ... }:

let
  unstable = import (fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz) {};
in

{
  imports = [
    ./hardware-configuration.nix

    # HOME-MANAGER
    "${ builtins.fetchTarball https://github.com/rycee/home-manager/archive/release-19.09.tar.gz }/nixos"
  ];

  # HARDWARE
  hardware = {
    cpu.intel.updateMicrocode = true;

    opengl.enable = true;
  };

  time.timeZone = "Europe/Rome";

  # BOOT
  boot = {
    cleanTmpDir = true;

    loader.grub.device = "/dev/sda";
  };

  # SYSTEM

  # gui
  services.xserver = {
    enable = true;

    layout = "it";
    xkbOptions = "eurosign:e";

    displayManager.gdm.enable = true;

    desktopManager.gnome3.enable = true;
    desktopManager.plasma5.enable = true;
  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "it";
    defaultLocale = "it_IT.UTF-8";
  };

  # network
  networking = {
    hostName = "ArataPC";

    networkmanager = {
      enable = true;
    };

    firewall = {
      enable = true;

      allowPing = true;

      allowedTCPPorts = [ 22 ];
      allowedUDPPorts = [ 22 ];
    };
  };

  services.sshd = {
    enable = true;
  };

  services.resolved.enable = true;

  # update
  nix = {
    gc = {
      automatic = true;
      dates = "daily";
      options = "--delete-older-than 30d";
    };

    optimise = {
      automatic = true;
      dates = [ "daily" ];
    };

    maxJobs = 2;
  };

  systemd.services.nixos-upgrade = {
    description = "NixOS Upgrade";

    restartIfChanged = false;
    unitConfig.X-StopOnRemoval = false;

    serviceConfig.Type = "oneshot";

    environment = config.nix.envVars //
                  {
                    inherit (config.environment.sessionVariables) NIX_PATH;
                    HOME = "/root";
                  } // config.networking.proxy.envVars;

    path = with pkgs; [ coreutils gnutar xz.bin gzip gitMinimal config.nix.package.out ];

    script = let
      nixos-rebuild = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild";
    in
      '' ${nixos-rebuild} boot --upgrade '';

    wants = ["network-online.target"];
    after = ["network-online.target"];

    startAt = "daily";
  };

  appstream.enable = true;

  environment.systemPackages = with pkgs; [

    xwayland

    ed
    file
    git
    htop
    inxi
    killall
    lshw
    lolcat
    mkpasswd
    neofetch
    parted
    pciutils
    tmux
    udisks
    usbutils
    wget

    gparted
  ];

  # USERS

  users.mutableUsers = false;

  users.users.utente = { pkgs, ... }: {
    extraGroups = [ "audio" "networkmanager" ];
    passwordFile = "users-utente";

    isNormalUser = true;
  };

  home-manager.users.utente = { pkgs, ...}: {

    programs.home-manager.enable = true;
    nixpkgs.config.allowUnfree = true;

    home.packages = with pkgs; [

      # Generici
      libreoffice-fresh

      spotify
      discord
      telegram
      sky

      firefox
      chromium

      # Programmazione

      # Editor
      neovim
      emacs
      gnome-builder
      geany

      # Git
      git
      smartgithg
      git-cola

      # C/C++
      gcc
      gdb
      gede
      cmake
      ninja
      meson

      # Go
      go
      gocode

      # Haskell
      ghc

      # Java
      jdk
      gradle
      gradle-completion
      eclipses.eclipse-java

      # LaTeX
      texlive.combined.scheme-small
      texstudio
      gnome-latex

      # Ocaml
      ocaml
      ocamlPackages.merlin

      # Prolog
      gprolog

      # Scheme
      guile

      # Python
      python3Full
      pypy3

      logisim

      # Elettronica
      octave

      # Meccanica
      freecad
      openscad
    ];

    programs.neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
    };

    programs.vscode = {
      enable = true;
      userSettings = {};
      extensions = [];
    };
  };
}
